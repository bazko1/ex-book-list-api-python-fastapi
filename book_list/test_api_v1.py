from os import environ
from typing import Optional

from fastapi.testclient import TestClient

from .config import AppConfig
from .service import Service

TEST_DATA = [
    ["The Foo", "Mr Bar", "2137", {}],
    ["The Python", "Mr Python", "2023", {}],
    ["The Sherlock Holmes", "Sir Arthur Conan Doyle", "2023", {}],
    ["Imaged", "Author", "2023", {
        "image": {
            "type": "base64",
            "data": "Zm9vYmFyCg=="}}],
    ["Described book", "Book Author", "3127",
        {"description": "The superb description of book."}],
    ["Partially Imaged", "Some Author", "2023", {
        "image": {
            "type": "base64",
            "data": "AAAAAAAAAAAAAAAAAAAA=="}}],
    ["Partially Imaged and described", "Some Author", "2023", {
        "image": {
            "type": "url",
            "data": "https://www.image-url.com"}}],
]

CFG_FILE = environ.get("CFG_FILE", default="./secrets/local.env.yaml")

config = AppConfig.parse_file(CFG_FILE)
service = Service(config)
client = TestClient(service.app)


def create_test_db():
    service.database.load_test_data(test_data=TEST_DATA)


# pylint: disable=too-many-arguments
def make_book_response_dict(title: str, author: str, year: str,
                            image: Optional[dict] = None,
                            id_: Optional[int] = None,
                            description: Optional[str] = None):
    out = {'title': title, 'author': author, 'year': year}
    if image:
        out['image'] = {}
        if 'type' in image:
            out['image']['type'] = image['type']
        if 'data' in image:
            out['image']['data'] = image['data']
    if id_:
        out['id'] = id_
    if description:
        out['description'] = description
    return out


test_data_indexed = [make_book_response_dict(t, a, y, id_=id_, **extra) for id_, (t, a, y, extra)
                     in enumerate(TEST_DATA, 1)]

test_create_book_data = {
    'title': 'POST Testing for dummies',
    'author': 'Unknown',
    'year': '2000'}


def get_book_by_id(id_: int):
    response = client.get(f"/v1/books/{id_}")
    return response


def test_get_health():
    response = client.get("/v1/health")
    assert response.status_code == 200
    assert response.json() == {
        "status": 200,
        "message": "ex-book-list-api-python api health",
        "content": {
            "alive": True,
            "postgres": True}}


def test_get_books():
    create_test_db()
    response = client.get("/v1/books")
    assert response.status_code == 200
    json_response = response.json()
    assert json_response['content']['count'] == len(test_data_indexed)
    assert json_response['content']['results'] == test_data_indexed


def test_get_existing_book():
    create_test_db()
    response = get_book_by_id(1)
    status = 200
    assert response.status_code == status
    assert response.json() == {'status': status,
                               'message': 'book',
                               'content': test_data_indexed[0]}


def test_get_non_existent():
    create_test_db()
    response = get_book_by_id(1234)
    status = 404
    assert response.status_code == status
    assert response.json() == {'status': status,
                               'message': 'there is no book with this id',
                               'content': None}


def test_create_book():
    service.database.drop_all_tables()
    service.database.migrate_database()
    response = client.post("/v1/books",
                           json=test_create_book_data)
    assert response.status_code == 201
    assert response.json() == {'status': 201,
                               'message': 'book: 1 created',
                               'content': {'id': 1}}


def test_create_image_description_book():
    service.database.drop_all_tables()
    service.database.migrate_database()
    data = test_create_book_data.copy()
    data.update({"image": {"data": "https://www.image-url.com",
                           "type": "url"},
                 "description": "The outstanding description 2."})
    response = client.post("/v1/books",
                           json=data)
    assert response.status_code == 201
    assert response.json() == {'status': 201,
                               'message': 'book: 1 created',
                               'content': {'id': 1}}
    data['id'] = 1
    book = get_book_by_id(1)
    assert book.json() == {'status': 200,
                           'message': 'book',
                           'content': data}


def test_create_partial_image_book():
    service.database.drop_all_tables()
    service.database.migrate_database()
    data = test_create_book_data.copy()
    data.update({"image": {"type": "url", "data": "https://www.image-url.com"}})
    response = client.post("/v1/books",
                           json=data)
    assert response.status_code == 201
    assert response.json() == {'status': 201,
                               'message': 'book: 1 created',
                               'content': {'id': 1}}
    data['id'] = 1
    book = get_book_by_id(1)
    assert book.json() == {'status': 200,
                           'message': 'book',
                           'content': data}


def test_update_book():
    test_create_book()
    updated_data = {'title': 'PUT Testing for dummies',
                    'author': 'The PUT guy',
                    'year': '2001',
                    'id': 1}
    response = client.put("/v1/books/1",
                          json=updated_data)
    assert response.status_code == 202
    assert response.json() == {
        'status': 202,
        'message': 'book',
        'content': updated_data}


def test_update_book_title():
    test_create_book()
    updated = {'title': 'PUT Testing for dummies', 'id': 1}
    content = test_create_book_data.copy()
    content.update(updated)
    response = client.put("/v1/books/1",
                          json=updated)
    assert response.status_code == 202
    assert response.json() == {
        'status': 202,
        'message': 'book',
        'content': content}


def test_update_add_image_description():
    test_create_book()
    updated = {'title': 'PUT Testing for dummies', 'id': 1,
               'image': {'type': 'base64', 'data': 'AABB=='},
               'description': 'Test Description'}
    content = test_create_book_data.copy()
    content.update(updated)
    response = client.put("/v1/books/1",
                          json=updated)
    assert response.status_code == 202
    assert response.json() == {
        'status': 202,
        'message': 'book',
        'content': content}


def test_post_empty_fields():
    response = client.post("/v1/books",
                           json={'title': '',
                                 'author': '',
                                 'year': ''})
    assert response.status_code == 406


def test_post_missing_fields():
    response = client.post("/v1/books",
                           json={'title': 'Title',
                                 'author': 'Author'})
    assert response.status_code == 406


def test_put_empty_fields():
    create_test_db()
    response = client.put("/v1/books/1",
                          json={'title': '',
                                'author': '',
                                'year': ''})
    assert response.status_code == 406


def test_delete_book():
    create_test_db()
    response = client.delete("/v1/books/1")
    assert response.status_code == 202
    assert response.json() == {
        "status": 202,
        "message": "book: 1 deleted",
        "content": {}
    }
