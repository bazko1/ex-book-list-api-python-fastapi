from fastapi import FastAPI
from fastapi.exceptions import RequestValidationError
from fastapi.middleware.cors import CORSMiddleware

from .config import PROJECT_TITLE, AppConfig
from .controller import BookController
from .crud import BookNotFoundException
from .database import Database
from .exc_handler import (no_book_exception_handler,
                          validation_exception_handler)
from .router import get_router
from .middleware import JsonContentMiddleware


class Service:
    def __init__(self, config: AppConfig, enable_cors: bool = False,
                 db_migrate: bool = False, load_test_data: bool = False):
        self.app = FastAPI(title=PROJECT_TITLE)
        self.config = config
        self.database = Database(self.config.Postgres.Url)
        self.handler = BookController(self.database)
        router_v1 = get_router(self.handler)
        self.app.include_router(router_v1)

        # add generic exception handler for BookNotFoundException and ValidationError
        self.app.add_exception_handler(BookNotFoundException, no_book_exception_handler)
        self.app.add_exception_handler(RequestValidationError, validation_exception_handler)

        self.app.add_middleware(JsonContentMiddleware)

        if enable_cors:
            self.app.add_middleware(
                CORSMiddleware,
                allow_origins=["*"],
                allow_credentials=True,
                allow_methods=["*"],
                allow_headers=["*"],
            )

        if db_migrate:
            self.database.migrate_database()

        if load_test_data:
            self.database.load_test_data()
