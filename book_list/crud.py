from typing import List

from sqlalchemy.orm import Session

import book_list.model.book as model
import book_list.schema.book as schema


class BookNotFoundException(Exception):
    pass


def get_books(db_session: Session, skip: int = 0, limit: int = 100) -> List[model.Book]:
    return db_session.query(model.Book).offset(skip).limit(limit).all()


def count_books(db_session: Session) -> int:
    return db_session.query(model.Book).count()


def get_book_by_id(db_session: Session, book_id: int) -> model.Book:
    book = db_session.query(model.Book).filter(model.Book.id == book_id).first()
    if not book:
        raise BookNotFoundException(f"Book with ID: '{book_id}' does not exist.")
    return book


def create_book(db_session: Session, book: schema.Book):
    image_info = {}
    if book.image:
        image_info = {'image_type': book.image.type.value,
                      'image_data': book.image.data}
    db_book = model.Book(title=book.title,
                         author=book.author,
                         year=book.year,
                         description=book.description,
                         **image_info)
    db_session.add(db_book)
    db_session.commit()
    db_session.refresh(db_book)
    return db_book


def delete_book(db_session: Session, book_id: int):
    db_book = get_book_by_id(db_session, book_id)
    db_session.delete(db_book)
    db_session.commit()
    return {"detail": f"book: {book_id} deleted"}


def update_book(db_session: Session, book_id: int, updates: schema.UpdateBook):
    db_book = get_book_by_id(db_session, book_id)
    book_dict = schema.Book.from_orm(db_book).dict(exclude_none=True)
    book_fields = updates.dict(exclude_none=True)
    book_dict.update(book_fields)
    # revalidate book schema
    schema.Book.validate(book_dict)
    if book_dict.get('image'):
        book_dict.update({'image_type': book_dict['image'].get('type').value,
                          'image_data': book_dict['image'].get('data')})
        del book_dict['image']
    for key, value in book_dict.items():
        setattr(db_book, key, value)
    db_session.commit()
    return db_book
