#!/usr/bin/env python3

import argparse
import sys

import uvicorn

from book_list.config import AppConfig
from book_list.database import Database
from book_list.logger import get_log_config
from book_list.service import Service

parser = argparse.ArgumentParser(prog="ex-book-list-api-python-fastapi")
subparsers = parser.add_subparsers(help="command", dest="command",
                                   required=True)

parser.add_argument("--config",
                    help="config file (default is ./secrets/local.env.yaml)",
                    default="./secrets/local.env.yaml")

subparsers.add_parser("migrate",
                      help="Migrate database")

subparsers.add_parser("load",
                      help="Migrate database and load test data")

serve_parser = subparsers.add_parser("serve",
                                     help="Starts service")
serve_parser.add_argument("--bind", "-b",
                          default="127.0.0.1",
                          help="This flag sets the IP to bind for our API server - overwrites config")
serve_parser.add_argument("--port", "-p",
                          default=8000,
                          help="This flag sets the port of our API server - overwrites config",
                          type=int)
serve_parser.add_argument("--migrate", "-m",
                          action=argparse.BooleanOptionalAction,
                          help="This decide if we should migrate or not when starting")
serve_parser.add_argument("--load", "-l",
                          action=argparse.BooleanOptionalAction,
                          help="This decide if we should load test data or not when starting")
serve_parser.add_argument("--cors", "-c",
                          action=argparse.BooleanOptionalAction,
                          help="This decide if we should enable cors - in the Prod NGiNX is solving cors for us")


def main():
    args = parser.parse_args()
    config = AppConfig.parse_file(args.config)

    if args.command == "serve":

        service = Service(config,
                          enable_cors=args.cors,
                          db_migrate=args.migrate,
                          load_test_data=args.load)
        log_config = get_log_config(config.Logger.Level.upper())
        uvicorn.run(service.app, host=args.bind,
                    port=args.port,
                    log_level=config.Logger.Level,
                    log_config=log_config)
    elif args.command == "migrate":
        dbase = Database(config.Postgres.Url)
        dbase.migrate_database()
    elif args.command == "load":
        dbase = Database(config.Postgres.Url)
        dbase.load_test_data()
    return 0


if __name__ == "__main__":
    sys.exit(main())
