from sqlalchemy import Column, Integer, String

from .base import Base


class Book(Base):
    __tablename__ = "books"
    id = Column(Integer, primary_key=True, index=True)
    title = Column(String, nullable=False)
    author = Column(String, nullable=False)
    year = Column(String, nullable=False)
    description = Column(String)
    image_type = Column(String)
    image_data = Column(String)
