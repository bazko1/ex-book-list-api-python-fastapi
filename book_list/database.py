from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

from book_list.model.base import Base
from book_list.model.book import Book

# default test data to have something to show/play with when db is created
DEFAULT_TEST_DATA = (
    ["The Foo", "Mr Bar", "2137", {}],
    ["The Python", "Mr Python", "2023", {}],
    ["The Sherlock Holmes", "Sir Arthur Conan Doyle", "2023", {}],
)


class Database:
    def __init__(self, uri: str) -> None:
        self.engine = create_engine(uri, pool_recycle=3600)
        self.session_maker = sessionmaker(autocommit=False, autoflush=False, bind=self.engine)

    def get_session(self):
        session = self.session_maker()
        try:
            return session
        finally:
            session.close()

    def migrate_database(self):
        Base.metadata.create_all(bind=self.engine)

    def drop_all_tables(self):
        Base.metadata.drop_all(bind=self.engine)

    def load_test_data(self, test_data: dict = DEFAULT_TEST_DATA):
        def make_book_model_dict(title, author, year, **extra_args):
            out = {'title': title, 'author': author, 'year': year}
            if 'image' in extra_args:
                out["image_type"] = extra_args['image'].get("type")
                out["image_data"] = extra_args['image'].get("data")
            out['description'] = extra_args.get('description')
            return out

        self.drop_all_tables()
        self.migrate_database()
        test_data_json = [make_book_model_dict(t, a, y, **extra) for t, a, y, extra in test_data]
        # create test data
        with self.get_session() as session:
            session.bulk_save_objects([
                Book(**json)
                for json in test_data_json
            ])
            session.commit()
