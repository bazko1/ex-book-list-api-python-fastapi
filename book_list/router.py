from fastapi import APIRouter, status

from book_list.controller import BookController


def get_router(handler: BookController):
    router = APIRouter(prefix="/v1")
    router.add_api_route("/health", handler.get_health, methods=["GET"])
    router.add_api_route("/books", handler.get_books, methods=["GET"],
                         response_model_exclude_none=True)
    router.add_api_route("/books/{book_id}", handler.get_book_by_id, methods=["GET"],
                         response_model_exclude_none=True)
    router.add_api_route("/books", handler.create_book, status_code=status.HTTP_201_CREATED, methods=["POST"],
                         response_model_exclude_none=True)
    router.add_api_route("/books/{book_id}", handler.update_book, status_code=status.HTTP_202_ACCEPTED, methods=["PUT"],
                         response_model_exclude_none=True)
    router.add_api_route("/books/{book_id}", handler.delete_book, status_code=status.HTTP_202_ACCEPTED,
                         methods=["DELETE"],
                         response_model_exclude_none=True)
    return router
