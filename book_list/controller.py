from typing import List

from fastapi import status
from fastapi.exceptions import RequestValidationError
from pydantic import NonNegativeInt, PositiveInt
from pydantic.error_wrappers import ValidationError
from sqlalchemy import exc, text

from book_list import crud
from book_list.schema.book import Book, BookResponse, UpdateBook
from book_list.schema.health import Health
from book_list.schema.response import PaginatedContent
from book_list.schema.response import Response as ResponseSchema


class BookController:
    def __init__(self, database):
        self.database = database

    async def get_health(self) -> ResponseSchema[Health]:
        with self.database.get_session() as db_session:
            ping = True
            try:
                db_session.execute(text("SELECT 1"))
            except exc.OperationalError:
                ping = False
            return ResponseSchema[Health](
                status=status.HTTP_200_OK,
                message="ex-book-list-api-python api health",
                content=Health(alive=True, postgres=ping))

    async def get_books(self, skip: NonNegativeInt = 0, limit: PositiveInt = 10) \
            -> ResponseSchema[PaginatedContent[List[BookResponse]]]:
        with self.database.get_session() as db_session:
            count = crud.count_books(db_session)
            books = crud.get_books(db_session, skip, limit)
            return ResponseSchema[PaginatedContent[List[BookResponse]]](
                status=status.HTTP_200_OK,
                message=f"books - skip: {skip}; limit: {limit}",
                content=PaginatedContent(count=count, skip=skip, limit=limit, results=books))

    async def get_book_by_id(self, book_id: int) -> ResponseSchema[BookResponse]:
        with self.database.get_session() as db_session:
            book = crud.get_book_by_id(db_session, book_id)
            return ResponseSchema[BookResponse](
                status=status.HTTP_200_OK,
                message="book",
                content=book)

    async def create_book(self, book: Book) -> ResponseSchema[dict]:
        with self.database.get_session() as db_session:
            new_book = crud.create_book(db_session, book)
            return ResponseSchema[dict](
                status=status.HTTP_201_CREATED,
                message=f"book: {new_book.id} created",
                content={"id": new_book.id})

    async def update_book(self, book_id: int, book: UpdateBook) -> ResponseSchema[BookResponse]:
        with self.database.get_session() as db_session:
            try:
                updated = crud.update_book(db_session, book_id, book)
            except ValidationError as error:
                raise RequestValidationError(error) from error
            return ResponseSchema[BookResponse](
                status=status.HTTP_202_ACCEPTED,
                message="book",
                content=updated)

    async def delete_book(self, book_id: int) -> ResponseSchema[dict]:
        with self.database.get_session() as db_session:
            crud.delete_book(db_session, book_id)
            return ResponseSchema[dict](
                status=status.HTTP_202_ACCEPTED,
                message=f"book: {book_id} deleted",
                content={})
