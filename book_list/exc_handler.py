
from fastapi import Request, status
from fastapi.responses import JSONResponse
from fastapi.exceptions import RequestValidationError

from book_list.crud import BookNotFoundException
from book_list.schema.response import Response as ResponseSchema


async def no_book_exception_handler(_: Request, __: BookNotFoundException) -> JSONResponse:
    return JSONResponse(dict(ResponseSchema[dict](status=status.HTTP_404_NOT_FOUND,
                                                  message="there is no book with this id",
                                                  content=None)),
                        status_code=status.HTTP_404_NOT_FOUND)


async def validation_exception_handler(_: Request, __: RequestValidationError) -> JSONResponse:
    return JSONResponse(dict(ResponseSchema[dict](status=status.HTTP_406_NOT_ACCEPTABLE,
                                                  message="please fill all the fields",
                                                  content=None)),
                        status_code=status.HTTP_406_NOT_ACCEPTABLE)
