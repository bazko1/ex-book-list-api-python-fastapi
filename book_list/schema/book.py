from enum import Enum
from typing import Optional

from pydantic import BaseModel, Field
from pydantic.main import ModelMetaclass


class ImageType(Enum):
    BASE64 = 'base64'
    URL = 'url'


class Image(BaseModel):
    type: ImageType | None = Field(title="Book image definition way type")
    data: str | None = Field(min_length=1, title="Book image base64", max_length=6000)


class Book(BaseModel):

    title: str = Field(min_length=1, title="Book title", max_length=300)
    author: str = Field(min_length=1, title="Book author", max_length=300)
    year: str = Field(min_length=1, title="Book print year", max_length=300)
    description: str | None = Field(min_length=1, title="Book Description", max_length=300)
    image: Image | None = Field(title="Book image information")

    class Config:
        orm_mode = True

    @classmethod
    def from_orm(cls, obj):
        """Construct Book object from orm model.
           All the fields except optional image should map one to one.
        """
        if (hasattr(obj, 'image_type') and obj.image_type is not None) or \
           (hasattr(obj, 'image_data') and obj.image_data is not None):
            obj.image = Image(type=obj.image_type,
                              data=obj.image_data)
        return super().from_orm(obj)


class BookResponse(Book):
    id: int | None = Field(title="Book id")


class AllOptional(ModelMetaclass):
    "Makes model fields to be all optional"
    def __new__(cls, name, bases, namespaces, **kwargs):
        annotations = namespaces.get('__annotations__', {})
        for base in bases:
            annotations.update(base.__annotations__)
        for field in annotations:
            if not field.startswith('__'):
                annotations[field] = Optional[annotations[field]]
        namespaces['__annotations__'] = annotations
        return super().__new__(cls, name, bases, namespaces, **kwargs)


class UpdateBook(Book, metaclass=AllOptional):
    pass
