from typing import Generic, Optional, TypeVar

from pydantic.generics import GenericModel

DataTypeT = TypeVar('DataTypeT')


class Response(GenericModel, Generic[DataTypeT]):
    status: int
    message: str
    content: Optional[DataTypeT]


class PaginatedContent(GenericModel, Generic[DataTypeT]):
    count: int
    skip: int
    limit: int
    results: DataTypeT
