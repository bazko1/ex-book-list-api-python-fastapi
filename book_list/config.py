
from typing import Optional

from pydantic import BaseModel, Field, PostgresDsn
from pydantic_yaml import YamlModel

PROJECT_TITLE = "ex-book-list-api"


class LoggerConfig(BaseModel):
    Level: str = Field(alias="level", default="info")


class PostgresConfig(BaseModel):
    # sqlalchemy connection does not permit scheme 'postgres://...'
    # even thought it is valid for most clients
    class AlchemyPostgresDns(PostgresDsn):
        allowed_schemes = PostgresDsn.allowed_schemes.difference({'postgres'})

    Url: AlchemyPostgresDns = Field(alias='url')


class ServerConfig(BaseModel):
    Host: Optional[str] = Field(alias='host')
    Port: Optional[int] = Field(alias='port')


class AppConfig(YamlModel):
    Env: str = Field(alias='env')
    Server: Optional[ServerConfig] = Field(alias='server')
    Logger: LoggerConfig = Field(alias='logger')
    Postgres: PostgresConfig = Field(alias='postgres')
