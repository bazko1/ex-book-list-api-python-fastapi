FROM python:3.10.9-slim-buster
WORKDIR /ex-book-list-api
COPY . .

RUN \
    apt-get update && \
    apt-get -y install libpq-dev gcc && \
    PBR_VERSION=1.2.3 pip3 install . && \
    apt-get clean

EXPOSE 8080

ENTRYPOINT ["ex-book-list-api-python-fastapi"]

CMD ["--config", "/secrets/local.env.yaml", "serve", "-b", "0.0.0.0", "-p", "8080", "-m"]

