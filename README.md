# ex-book-list-api-python

### Running locally

#### Dependencies:
You need python3.10+.
Postgresql and gcc is also required for debian like systems you can install by running:
```
apt install libpq-dev gcc
```

#### Generating secrets
The example password is `ThisIsExamplePassword4U` - run:
```
./local-configure.sh
```

#### As package:
Project can be installed as package with virtual env.
Install python3 and venv first.

```
# create virtual env under preferred location 
python3 -m venv fastapienv
# activate virtual env
. ./fastapienv/bin/activate
# from repo root directory call 
pip3 install .
```

After that your env `fastapienv` will contain command `ex-book-list-api-python-fastapi`.

#### Independently from source:
Have python3 installed.
```
# install requirements
pip3 install -r requirements.txt
python3 run.py <args>
```

### Running tests:
```
# install test requirements
pip3 install -r test-requirements.txt
# call test with proper CFG_FILE
CFG_FILE="$(pwd)/secrets/local.env.yaml" pytest -v book_list
```